module Main where

import Lib

main = print(listMap listToString (NotEmptyList 0 (NotEmptyList 1 (EmptyList))))

isLessThan :: Int -> Int -> Bool
isLessThan n t = n < t

twoLessThan = isLessThan 2

factors :: Int -> [Int]
factors n = [x | x <- [1..n], n `mod` x == 0]

properDivisors :: Int -> [Int]
properDivisors n = init(factors n)

prime :: Int -> Bool
prime n = factors n == [1, n]

isPerfect :: Int -> Bool
isPerfect n = sum(properDivisors n) == n

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial(n -1)

fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib i = fib (i - 1) + fib (i - 2)

data PBool = PFalse | PTrue

toString :: PBool -> String
toString PTrue = "PTrue"
toString PFalse = "PFalse"

pNot :: PBool -> PBool
pNot PTrue = PFalse
pNot PFalse = PTrue

pOr :: PBool -> PBool -> PBool
pOr PTrue _ = PTrue
pOr PFalse p = p

pAnd :: PBool -> PBool -> PBool
pAnd PTrue PTrue = PTrue
pAnd _ _ = PFalse

data PInt = PZero | PSuccessor PInt

toInt :: PInt -> Int
toInt PZero = 0
toInt (PSuccessor p) = 1 + (toInt p)

fromInt :: Int -> PInt
fromInt 0 = PZero
fromInt i = PSuccessor (fromInt (i - 1))

addOne :: PInt -> PInt
addOne p = PSuccessor p

plus :: PInt -> PInt -> PInt
plus p (PSuccessor q) = PSuccessor(plus p q)
plus (PSuccessor p) q = PSuccessor(plus p q)
plus PZero PZero = PZero

takeOne :: PInt -> PInt
takeOne (PSuccessor p) = p

-- solve this beyotch
-- minus :: PInt -> PInt -> PInt
-- minus p (PSuccessor q) =
-- minus (PSuccessor p) q =
-- minus p PZero = p
-- minus PZero _ = PZero

-- Lists

-- todo: how do we add identifier names for NotEmptyList constructor?
data SinglyLinkedList = EmptyList | NotEmptyList Int SinglyLinkedList

listToString :: SinglyLinkedList -> String
listToString EmptyList = "Empty List"
listToString (NotEmptyList head tail) = (show head) ++ ", " ++ (listToString tail)

listLength :: SinglyLinkedList -> Int
listLength EmptyList = 0
listLength (NotEmptyList _ tail) = 1 + (listLength tail)

listMap :: (SinglyLinkedList -> String) -> SinglyLinkedList -> [String]
listMap _ EmptyList = [""]
listMap f (NotEmptyList head tail) = concat (f (show head)) (listMap tail)

listFilter :: (SinglyLinkedList -> SinglyLinkedList) -> SinglyLinkedList -> SinglyLinkedList
listFilter _ EmptyList = EmptyList
listFilter f (NotEmptyList head tail) =
-- write some array functions, beyotch: map, filter, length, reduce

-- subtraction (no negative results)

-- multiplication

-- exponentiation

-- foldr (using PInts or Lists)
